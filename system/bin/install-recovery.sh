#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:55797036:a7ceb2b1b23d6eecf6aed54eccc36468fc19b8af; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:51741992:9a23dc02a506d9db501631c55a815649844df883 EMMC:/dev/block/bootdevice/by-name/recovery a7ceb2b1b23d6eecf6aed54eccc36468fc19b8af 55797036 9a23dc02a506d9db501631c55a815649844df883:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
